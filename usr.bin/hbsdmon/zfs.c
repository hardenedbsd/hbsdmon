/*-
 * Copyright (c) 2021 HardenedBSD Foundation Corp.
 * Author: Shawn Webb <shawn.webb@hardenedbsd.org>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

#include <sys/param.h>
#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <unistd.h>

#include <sys/queue.h>
#include <sys/stat.h>

#include <stdint.h>
#include <sys/types.h>

#include "hbsdmon.h"

#include "liblattzfs.h"

bool
hbsdmon_zfs_init(hbsdmon_node_t *node)
{
	hbsdmon_keyvalue_t *kv;
	lattutil_log_t *logp;
	lattzfs_ctx_t *ctx;
	char *pool;

	if (node == NULL) {
		return (false);
	}

	logp = hbsdmon_thread_get_logger(node->hn_thread);

	kv = hbsdmon_find_kv_in_node(node, "pool", false);
	if (kv == NULL) {
		return (true);
	}
	pool = hbsdmon_keyvalue_to_str(kv);
	if (pool == NULL) {
		return (false);
	}

	ctx = lattzfs_ctx_new(pool, 0);
	if (ctx == NULL) {
		logp->ll_log_err(logp, -1, "lattzfs_ctx_new failed");
		return (false);
	}

	kv = hbsdmon_new_keyvalue();
	if (kv == NULL) {
		logp->ll_log_err(logp, -1, "Unable to create keyvalue");
		return (false);
	}

	if (!hbsdmon_keyvalue_store(kv, "zfsctx", ctx,
	    sizeof(*ctx))) {
		logp->ll_log_err(logp, -1, "Cannot store zfs ctx");
		return (false);
	}
	hbsdmon_node_append_kv(node, kv);

	return (true);
}

bool
hbsdmon_zfs_status(hbsdmon_node_t *node)
{
	lattzfs_zpool_status_t status;
	lattzfs_zpool_errata_t errata;
	hbsdmon_keyvalue_t *kv;
	lattutil_log_t *logp;
	lattzfs_ctx_t *ctx;

	if (node == NULL) {
		return (false);
	}

	logp = hbsdmon_thread_get_logger(node->hn_thread);

	kv = hbsdmon_find_kv_in_node(node, "zfsctx", false);
	if (kv == NULL) {
		logp->ll_log_err(logp, -1, "zfsctx not set");
		return (true);
	}

	ctx = ((lattzfs_ctx_t *)(kv->hk_value));

	errata = 0;
	status = 0;
	if (!lattzfs_zpool_get_status(ctx, &status, &errata)) {
		logp->ll_log_err(logp, -1, "Cannot get zpool %s status",
		    lattzfs_get_pool_name(ctx));
		return (false);
	}

	switch (status) {
	case LATT_ZPOOL_STATUS_OK:
	case LATT_ZPOOL_STATUS_VERSION_OLDER:
	case LATT_ZPOOL_STATUS_FEAT_DISABLED:
	case LATT_ZPOOL_STATUS_COMPATIBILITY_ERR:
		return (true);
	default:
		return (false);
	}
}
