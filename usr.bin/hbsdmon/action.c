/*-
 * Copyright (c) 2024 HardenedBSD Foundation Corp.
 * Author: Shawn Webb <shawn.webb@hardenedbsd.org>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

#include <assert.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <ucl.h>

#include "hbsdmon.h"

#include <sys/types.h>
#include <sys/sbuf.h>
#include <sys/wait.h>

extern char **environ;

static char ** build_args(hbsdmon_action_t *);

hbsdmon_action_t *
hbsdmon_action_new(hbsdmon_action_when_t when)
{
	hbsdmon_action_t *action;

	if (!hbsdmon_action_when_valid(when)) {
		return (NULL);
	}

	action = calloc(1, sizeof(*action));
	if (action == NULL) {
		return (NULL);
	}

	action->ha_when = when;

	return (action);
}

void
hbsdmon_action_free(hbsdmon_action_t **actionp)
{
	hbsdmon_action_t *action;

	if (actionp == NULL || *actionp == NULL) {
		return;
	}

	action = *actionp;
	*actionp = NULL;
	/* TODO: Free internal bits */
	free(action);
}

bool
hbsdmon_action_when_valid(hbsdmon_action_when_t when)
{
	switch (when) {
	case ACTION_WHEN_SUCCESS:
	case ACTION_WHEN_FAIL:
		return (true);
	default:
		return (false);
	}
}

hbsdmon_action_when_t
hbsdmon_action_when_from_string(const char *when)
{
	if (when == NULL) {
		return (ACTION_WHEN_UNKNOWN);
	}

	if (!strcasecmp(when, "success")) {
		return (ACTION_WHEN_SUCCESS);
	}
	if (!strcasecmp(when, "fail")) {
		return (ACTION_WHEN_FAIL);
	}

	return (ACTION_WHEN_UNKNOWN);
}

const char *
hbsdmon_action_when_to_string(hbsdmon_action_when_t when)
{
	switch (when) {
	case ACTION_WHEN_SUCCESS:
		return ("success");
	case ACTION_WHEN_FAIL:
		return ("fail");
	default:
		return ("unknown");
	}
}

bool
hbsdmon_action_add_argument(hbsdmon_action_t *action, const char *arg)
{
	char **args;

	if (action == NULL || arg == NULL) {
		return (false);
	}

	args = realloc(action->ha_args, sizeof(char **) *
	    (action->ha_nargs + 1));
	if (args == NULL) {
		return (false);
	}

	action->ha_args = args;
	args[action->ha_nargs] = strdup(arg);
	if (args[action->ha_nargs] == NULL) {
		return (false);
	}

	action->ha_nargs++;
	return (true);
}

bool
hbsdmon_action_exec_all(hbsdmon_ctx_t *ctx, hbsdmon_node_t *node,
    hbsdmon_action_when_t when)
{
	hbsdmon_action_t *action, *actiontmp;

	if (ctx == NULL || node == NULL || !hbsdmon_action_when_valid(when)) {
		return (false);
	}

	SLIST_FOREACH_SAFE(action, &(node->hn_actions), ha_entry, actiontmp) {
		if (action->ha_when != when) {
			continue;
		}

		if (!hbsdmon_action_exec(ctx, node, action)) {
			return (false);
		}
	}

	return (true);
}

bool
hbsdmon_action_exec(hbsdmon_ctx_t *ctx, hbsdmon_node_t *node,
    hbsdmon_action_t *action)
{
	char **args;
	int status;
	pid_t pid;

	if (ctx == NULL || node == NULL || action == NULL) {
		return (false);
	}

	pid = fork();
	switch (pid) {
	case -1:
		return (false);
	case 0:
		args = build_args(action);
		if (args == NULL) {
			return (false);
		}

		chdir(action->ha_cwd != NULL ? action->ha_cwd :
		    HBSDMON_DEFAULT_ACTION_CWD);
		execve(args[0], args, environ);
		free(args);
		return (false);
	default:
		status = 0;
		if (waitpid(pid, &status, 0) == -1) {
			perror("waitpid");
			return (false);
		}
	}

	return (true);
}

static char **
build_args(hbsdmon_action_t *action)
{
	char **res;

	res = realloc(action->ha_args, sizeof(char **) *
	    (action->ha_nargs + 1));
	if (res == NULL) {
		return (NULL);
	}

	res[action->ha_nargs] = NULL;

	return (res);
}
