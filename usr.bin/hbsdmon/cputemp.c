/*-
 * Copyright (c) 2024 HardenedBSD Foundation Corp.
 * Author: Shawn Webb <shawn.webb@hardenedbsd.org>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <sys/types.h>
#include <sys/socket.h>
#include <sys/sysctl.h>
#include <netdb.h>

#include <ucl.h>

#include "hbsdmon.h"

float
hbsdmon_cputemp_gettemp(const char *name)
{
	float base, fval;
	unsigned int val;
	size_t len;

	val = 0;
	len = sizeof(val);
	if (sysctlbyname(name, &val, &len, NULL, 0)) {
		perror("[-] sysctlbyname");
		return (0.0);
	}

	base = 10.0;
	fval = (float)val;
	fval /= base;
	fval -= 273.15;

	return (fval);
}

bool
hbsdmon_cputemp_status(hbsdmon_node_t *node)
{
	hbsdmon_keyvalue_t *kv;
	unsigned int kvlimit;
	float limit, val;
	char *name;

	kv = hbsdmon_find_kv_in_node(node, "sysctl", false);
	if (kv == NULL) {
		return (false);
	}

	name = hbsdmon_keyvalue_to_str(kv);
	if (name == NULL) {
		return (false);
	}

	val = hbsdmon_cputemp_gettemp(name);
	if (val == 0.0) {
		return (false);
	}

	limit = 65.0;
	kv = hbsdmon_find_kv_in_node(node, "limit", false);
	if (kv != NULL) {
		kvlimit = hbsdmon_keyvalue_to_int(kv);
		limit = (float)kvlimit;
	}

	if (val >= limit) {
		return (false);
	}

	return (true);
}
