/*-
 * Copyright (c) 2021 HardenedBSD Foundation Corp.
 * Author: Shawn Webb <shawn.webb@hardenedbsd.org>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <errno.h>

#include <sys/param.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/sysctl.h>
#include <netdb.h>

#include <vm/vm_param.h>

#include <ucl.h>

#include "hbsdmon.h"

bool
hbsdmon_swap_status(hbsdmon_node_t *node)
{
	lattutil_log_t *logger;
	struct xswdev xsw;
	size_t mibsz, sz;
	int mib[16];

	memset(&xsw, 0, sizeof(xsw));
	logger = hbsdmon_thread_get_logger(node->hn_thread);

	mibsz = nitems(mib);
	if (sysctlnametomib("vm.swap_info", mib, &mibsz)) {
		logger->ll_log_err(logger, -1,
		    "sysctlnametomib: %s", strerror(errno));
		return (false);
	}

	sz = sizeof(xsw);
	mib[mibsz] = 0;
	if (sysctl(mib, mibsz + 1, &xsw, &sz, NULL, 0)) {
		logger->ll_log_err(logger, -1,
		    "sysctl: %s", strerror(errno));
		return (false);
	}

	if (xsw.xsw_used) {
		return (false);
	}

	return (true);
}
